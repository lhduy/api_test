import os
import re
from tqdm import tqdm
import numpy as np
import pandas as pd
import torch
from utils import preprocessing_for_bert
from torch.utils.data import TensorDataset, DataLoader, RandomSampler, SequentialSampler

def loadDataFromFolder(path):
    files =[]
    for r,d,f in os.walk(path):
        for file in f:
            if ('.txt' in file):
                exact_path = os.path.join(r,file)
                files.append(exact_path)
    texts = []
    classes = []
    print(files)
    id = 0
    for file in files:
        with open(file, "r") as fd:
            for line in fd:
                texts.append(line.strip())
                classes.append(id)
        id+=1
    data = {'sequence': texts, 'label': classes}
    data = pd.DataFrame(data=data)
    return data


def splitTrainVal(data):
    from sklearn.model_selection import train_test_split
    X = data.sequence.values
    y = data.label.values
    X_train, X_val, y_train, y_val = train_test_split(X, y, test_size=0.1, random_state=2020)
    return X_train, X_val, y_train, y_val


def createDataLoader(config):
    from transformers import BertJapaneseTokenizer
    tokenizer = BertJapaneseTokenizer.from_pretrained("cl-tohoku/bert-base-japanese-v2")
    
    data = loadDataFromFolder(config['train_data'])

    all_tweets = np.concatenate([data.sequence.values])
    encoded_tweets = [tokenizer.encode(sent, add_special_tokens=True) for sent in all_tweets]
    max_len = max([len(sent) for sent in encoded_tweets])
    print('Max length: ', max_len)
    
    # Encode our concatenated data
    X_train, X_val, y_train, y_val = splitTrainVal(data)
    print(X_train.shape, X_val.shape, y_train.shape, y_val.shape)

    train_inputs, train_masks = preprocessing_for_bert(X_train,max_len+4)
    val_inputs, val_masks = preprocessing_for_bert(X_val,max_len+4)

    train_labels = torch.tensor(y_train)
    val_labels = torch.tensor(y_val)
    print(train_inputs.shape, train_masks.shape, val_inputs.shape, val_masks.shape,train_labels.shape,val_labels.shape)
    # Create the DataLoader for our training set
    train_data = TensorDataset(train_inputs, train_masks, train_labels)
    train_sampler = RandomSampler(train_data)
    train_dataloader = DataLoader(train_data, sampler=train_sampler, batch_size=config['train']['batch_size'])

    # Create the DataLoader for our validation set
    val_data = TensorDataset(val_inputs, val_masks, val_labels)
    val_sampler = SequentialSampler(val_data)
    val_dataloader = DataLoader(val_data, sampler=val_sampler, batch_size=config['train']['batch_size'])

    return train_dataloader, val_dataloader
