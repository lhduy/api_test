from .model import BertClassifier 
from transformers import BertJapaneseTokenizer
import torch

class ClassficationModel():
    def __init__(self,model_path = './model_3classes.pth'):
        config ={
            'hidden_node':50,
            'num_classes':3,
            'train_data':'Data/3-class',
            'train':
                {
                    'lr':5e-5,
                    'eps':1e-8,
                    'num_epoch':3,
                    'batch_size':32
                },
            'device': torch.device("cuda"),
            
            }
        self.model = BertClassifier(num_classes= config['num_classes'],hidden_node=config['hidden_node'],freeze_bert=True)
        self.model.to(config['device'])
        self.model.load_state_dict(torch.load(model_path))
        self.model.eval()
        self.tokenizer = BertJapaneseTokenizer.from_pretrained("cl-tohoku/bert-base-japanese-v2")
        self.device = config['device']

    def preprocessing(self,text):
        input_ids = []
        attention_masks = []
        encoded_sent = self.tokenizer.encode_plus(
                # text=text_preprocessing(sent),  # Preprocess sentence
                text=text,  # Preprocess sentence
                add_special_tokens=True,        # Add `[CLS]` and `[SEP]`
                max_length=65,                  # Max length to truncate/pad
                pad_to_max_length=True,         # Pad sentence to max length
                truncation=True,
                #return_tensors='pt',           # Return PyTorch tensor
                return_attention_mask=True      # Return attention mask
        )
        input_ids= encoded_sent.get('input_ids')
        attention_masks=encoded_sent.get('attention_mask')
        input_ids = torch.tensor(input_ids).to(self.device)
        input_ids = torch.unsqueeze(input_ids,0)
        attention_masks = torch.tensor(attention_masks).to(self.device)
        attention_masks = torch.unsqueeze(attention_masks,0)
        # print(input_ids.shape,attention_masks.shape)
        return input_ids, attention_masks
    def __call__(self,input_text):
        input_ids, attn_mask = self.preprocessing(input_text)
        with torch.no_grad():
            logits = self.model(input_ids, attn_mask)
        preds = torch.argmax(logits, dim=1).flatten()
        # print(logits)
        # probs = softmax(logits,dims = 1).flatten()
        # print(probs)
        if preds == 0:
            return 'OK',logits
        elif preds ==1:
            return 'NG',logits
        elif preds == 2:
            return 'warning',logits

if __name__ == '__main__':
    test_model = ClassficationModel()
    print(test_model('もちろん送料も無料'))
    print(test_model('私も買おうとしら品切れ。'))
    print(test_model('肌生まれ変わった'))
    print(test_model('検証結果で効果が実証されました'))
    print(test_model('エステに行く必要がなくなりました'))
    print(test_model('２週間という異常なスピードでシワやほうれい線けしを可能に'))
        
