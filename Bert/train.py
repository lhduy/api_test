from transformers import AdamW, get_linear_schedule_with_warmup
import torch.nn as nn
import torch
from DataLoader import createDataLoader
from model import BertClassifier 
from utils import train
def initialize_model(train_dataloader,config):
    """Initialize the Bert Classifier, the optimizer and the learning rate scheduler.
    """
    bert_classifier = BertClassifier(num_classes= config['num_classes'],hidden_node=config['hidden_node'],freeze_bert=False)
    bert_classifier.to(config['device'])
    optimizer = AdamW(bert_classifier.parameters(),
                      lr=config['train']['lr'],    # Default learning rate
                      eps=config['train']['eps']    # Default epsilon value
                      )

    total_steps = len(train_dataloader) * config['train']['num_epoch']

    scheduler = get_linear_schedule_with_warmup(optimizer,
                                                num_warmup_steps=0, # Default value
                                                num_training_steps=total_steps)
    return bert_classifier, optimizer, scheduler



def run():
    config ={
        'hidden_node':50,
        'num_classes':3,
        'train_data':'Data/3-class',
        'train':
            {
                'lr':5e-5,
                'eps':1e-8,
                'num_epoch':3,
                'batch_size':32
            },
        'device': torch.device("cpu"),
        
    }
    # Load Data and create DataLoader
    # Convert other data types to torch.Tensor

    train_dataloader, val_dataloader = createDataLoader(config)


    bert_classifier, optimizer, scheduler = initialize_model(train_dataloader,config)
    
    loss_fn = nn.CrossEntropyLoss()
    # train(config,bert_classifier,loss_fn, train_dataloader, val_dataloader, epochs=3, evaluation=True)
    train(config=config,model=bert_classifier,optimizer=optimizer,scheduler=scheduler,loss_fn=loss_fn, train_dataloader=train_dataloader, val_dataloader=val_dataloader,     epochs=5, evaluation=True)
    torch.save(bert_classifier.state_dict(), 'model_3classes.pth')
def main():
    run()

if __name__ == '__main__':
    main()
