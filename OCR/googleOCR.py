import json
import numpy as np
import requests
from base64 import b64encode

def makeImageData(imgpath):
    img_req = None
    with open(imgpath, 'rb') as f:
        ctxt = b64encode(f.read()).decode()
        img_req = {
            'image': {
                'content': ctxt
            },
            'features': [{
                'type': 'DOCUMENT_TEXT_DETECTION',
                'maxResults': 1
            }]
        }
    return json.dumps({"requests": img_req}).encode()
def requestOCR(imgpath):
    imgdata = makeImageData(imgpath)
    ENDPOINT_URL = 'https://vision.googleapis.com/v1/images:annotate'
    api_key = "AIzaSyCmYk1BTmue2nljqJYD_tSl5HgJZd5kR5s"
    response = requests.post(ENDPOINT_URL, 
                            data = imgdata, 
                            params = {'key': api_key}, 
                            headers = {'Content-Type': 'application/json'})
    return response

# def gen_cord(result):
#     cord_df = pd.DataFrame(result['boundingPoly']['vertices'])
#     x_min, y_min = np.min(cord_df["x"]), np.min(cord_df["y"])
#     x_max, y_max = np.max(cord_df["x"]), np.max(cord_df["y"])
#     return result["description"], x_max, x_min, y_max, y_min

def googleOCR(img_loc):
    result = requestOCR(img_loc)
    final_return = []

    if result.status_code != 200 or result.json().get('error'):
        print ("Error code 220")
    else:
        try:
            result = result.json()['responses'][0]['fullTextAnnotation']['pages'][0]['blocks']
        except:
            result = []

    for index in range(len(result)):
        t_bbox = []
        temp_bb = result[index]['boundingBox']['vertices']
        for index_bb in range(len(temp_bb)):
            t_bbox.append([int(temp_bb[index_bb]['y']),int(temp_bb[index_bb]['x'])])
        t_bbox = np.array(t_bbox)
        # loc =np.mean(t_bbox,axis =0)
        temp = ''
        words = result[index]['paragraphs'] 
        for index_words in range(len(words)):
            symbols = words[index_words]['words']
            for index_symbols in range(len(symbols)):
                text = symbols[index_symbols]['symbols']
                for index_text in range(len(text)):
                    temp = temp + text[index_text]['text']
        final_return.append([t_bbox,temp])
    return final_return

