import easyocr
import cv2
import json
import numpy as np


image_file_extention = ['jpg','JPG','jpeg','JPEG','png','PNG','bmp','BMP','tiff','TIFF']

class OCRProcessing():
    def __init__(self,lang='ja'):
        self.ocr_reader = easyocr.Reader([lang])

    def __call__(self,input_file):
        if isinstance(input_file,str):
            # temp = cv2.imread(input_file)
            # w,h,c = temp.shape
            # print(input_file,any(input_file.endswith(ext) for ext in image_file_extention))
            if any(input_file.endswith(ext) for ext in image_file_extention):
                try:
                    return_data = self.process(input_file)
                    return return_data
                # except:
                except:
                    return []
            else:
                return []
                # return self.postProcessing(return_data)
        else:
            return []
    def process(self,input_file):
        return self.ocr_reader.readtext(input_file)

    def postProcessing(self,return_data):
        temp =  []
        for data in return_data:
            json_temp = {}
            json_temp['loc'] = np.mean(np.array(data[0],dtype=np.int32).tolist(),axis =0)
            json_temp['text'] = data[1]
            json_temp['conf'] = data[2]
            temp.append(json_temp)
        # json_data ={}
        json_data['OCR_data'] = temp
        # return json.dumps(json_data)
        return json_data

            
        
def main():
    # import pprint
    ocr_reader = OCRProcessing()
    dd = ocr_reader('d668b3d26234ebec2cd70d70b1d98b74.jpg')
    # pprint(dd)
    print(dd)
if __name__=='__main__':
    main()
