## install python
    using python version 3 (>3.7). Download from https://www.python.org/ 
## Required packet 
- Flask 
- werkzeug

Install all packets: pip install -r requirement.txt

## Run sample
- cd to api_test folder
- python run.py
- using localhost:5000/ for uploader api testing
- using localhost:5000/server for admin api testing
