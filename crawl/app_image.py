import os
import shutil
import argparse
import logging
from multiprocessing import Pool
import requests
import numpy as np
import requests
from bs4 import BeautifulSoup
from urllib.request import urlopen

# =============================================================================

def get_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--url', '-l', type=str,
            default='https://weekly.ascii.jp/',
            help='URL to crawl')
    parser.add_argument('--images', '-i', type=str,
            required=False, default='images',
            help='Path to image folder for storage')

    return parser.parse_args()

# =============================================================================

def crawl_image(args):
    image_url, images_folder = args
    _, image_fname = os.path.split(image_url)
    
    try:
        response = requests.get(image_url, stream=True)
        # print(image_url, 'done')
        with open(f'{images_folder}{os.sep}{image_fname}', 'wb') as f:
            shutil.copyfileobj(response.raw, f)
        del response
    except Exception as e:
        print(str(e))

# =============================================================================

def app(url, images_folder):
    os.makedirs(images_folder, exist_ok=True)
    html = requests.get(url)
    print('HTML Reading Done\n')
    soup = BeautifulSoup(html.text, features="html.parser")

    # crawl text
    for script in soup(["script", "style"]):
        script.extract()    

    text = soup.get_text()
    lines = (line.strip() for line in text.splitlines())
    chunks = (phrase.strip() for line in lines for phrase in line.split("  "))
    text = '\n'.join(chunk for chunk in chunks if chunk)
    # ---------------
    # print('Before delete')
    files =[]
    for r,d,f in os.walk('temp_data'):
        for file in f:
            exact_path = os.path.join(r,file)
            files.append(exact_path)
    # print(files)
    for f in files:
        os.remove(f)
    # ---------------
    # print('After delete')
    files =[]
    for r,d,f in os.walk('temp_data'):
        for file in f:
            exact_path = os.path.join(r,file)
            files.append(exact_path)
    print(text)
    
    # ---------------
    with open('temp_data/html.txt', 'w') as f:
        f.write(text)

    # crawl img
    tags = soup.find_all('img')
    
    image_urls = []

    for tag in tags:
        if 'src' in tag.attrs:
            image_url = tag['src']
            if image_url.endswith('.svg'): continue
            # image_url = image_url[:,image_url.find('?')]
            image_urls.append([image_url, images_folder])

            if 'data-src' in tag.attrs:
                image_url = tag['data-src']
                if image_url.endswith('.svg'): continue
                # image_url = image_url[:,image_url.find('?')]
                image_urls.append([image_url, images_folder])
        # print(image_urls)
    print('Image URLs Crawling Done\n')
    print('Num. of Image URLs:', len(image_urls))

    with Pool(4) as p:
        p.map(crawl_image, image_urls)
        p.terminate()
        p.join()

# =============================================================================

def main(args):
    app(args.url, args.images)

# =============================================================================

if __name__ == '__main__':
    logging.info('Task: Crawl Image\n')
    args = get_args()
    main(args)
    logging.info('Process Done')
