from flask import Flask, render_template, request, redirect, url_for, jsonify
from werkzeug.utils import secure_filename
from utils import json_generator,json_generator_admin
import json
app = Flask(__name__)


@app.route('/')
def index():
    return render_template('/uploader.html')

@app.route('/return', methods = ['GET', 'POST'])
def upload_file():
    if request.method == 'POST':
        incoming_url = request.form.get('url')
        if incoming_url:
            incoming_url=incoming_url.strip()
        
        try:
            print('Get raw data, no URL')
            incoming_text_list = request.get_data()
            my_json = incoming_text_list.decode('utf8')
            data = json.loads(my_json)
            # print(data)
            
            if 'text' in data:
                data_text = data['text']
                print("Get Text data with length: {}".format(len(data_text)))
            else:
                data_text = None

            if 's3_image' in data:
                s3_image = data['s3_image']
                print("get image from S3: {}".format(s3_image))

            else:
                s3_image = None
            if 'env' in data:
                env = data['env']
                print("Enviroment: {}".format(env))

            else:
                env = None
                
        except:
            data_text = None
            env = None
            s3_image = None
            pass
        
        try:
            uploaded_file = request.files['file']
            
            filename = secure_filename(uploaded_file.filename)
            print("get image from form: {}".format(filename))
            if uploaded_file.filename != '':
                uploaded_file.save(filename)
        except:
            filename = None
            pass
        
        data = json_generator(incoming_url,filename,data_text,s3_image,env)
        
        return jsonify(
            data
        )

@app.route('/server')
def indexServer():
    return render_template('/server.html')

@app.route('/returnServer', methods = ['GET', 'POST'])
def returnServer():
    if request.method == 'POST':
        input_data = request.form.get('data_check')
        recomendation = request.form.get('recomendation')
        reasons = request.form.get('reason')
        category = request.form.get('category')
        data = {'caterogry':category,
            'data_check':input_data,
            'recomendation':recomendation,
            'reasons': reasons,
            'result':'Not Given'
            }
        return jsonify(
            data
        )
        
if __name__ == '__main__':
    app.run(host = '0.0.0.0',debug = True)
