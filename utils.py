import os
import json
import re
import numpy as np
from werkzeug.utils import import_string
from OCR.googleOCR import googleOCR
from Bert.inference_3classes import ClassficationModel
from crawl.app_image import app as crawl_web
# import numpy as np
import torch
from torch.nn.functional import softmax
from uploaderS3 import upload_to_aws
from downloadS3 import download_aws
text_model = ClassficationModel(model_path = './Bert/model_3classes.pth')
# ocr_reader = OCRProcessing()
ocr_reader = googleOCR

# def softmax(x):
#     """Compute softmax values for each sets of scores in x."""
#     e_x = np.exp(x - np.max(x))
#     return e_x / e_x.sum()
    
# check input file is IMAGE FILE or DOC FILE
def file_ext_validator(filename):
    image_file_extention = ['jpg','JPG','jpeg','JPEG','png','PNG','bmp','BMP','tiff','TIFF']
    doc_file_extention = ['word','WORD','doc','DOC','docx','DOCX','xsl','DOCX','xslx','XSLS','pdf','PDF','txt','TXT']
    
    return_type = None
    filename = filename.lower() 
    if any(filename.endswith(ext) for ext in image_file_extention):
        return_type = 'IMAGE'
    elif any(filename.endswith(ext) for ext in doc_file_extention):
        return_type = 'FILE'
    return return_type

def check_upload(filename):
    return_type = None
    if isinstance(filename, str):

            # file_ext = os.path.splitext(filename)[1]
        return_type = file_ext_validator(filename)
    # print(file_ext)
    return return_type

def json_generator(incoming_string,file_name,data_text,s3_image,env='prod'):

    num_NG = 0
    num_OK = 0
    num_warning = 0 
    # ------------------- FILES ----------------

    if file_name:
        content_type = check_upload(file_name)
        print('filename:{} - Detect type: {}'.format(file_name, content_type))
    else:
        content_type = None
    # ------------------- DUMP DATA ----------------
    
    data = {'file_type':'URL',
            'url_data':{
                'web_url':'',
                'image_url':'',
                'file_url':'' #for DOC file
                },
            'count': 0,
            'ng_count':0,
            "warning_count": 0,
            'screen_shot': 'https://vibooking.vn/wp-content/uploads/2019/07/Sunrise-Premium-Resort-Hoi-An-1-870x555.jpg',
            'response':'',
            'output':[]
            }
    # ------------------- URL ----------------
    
    if incoming_string and (incoming_string.startswith('https://') or incoming_string.startswith('http://')):
        data['url_data']['web_url'] = incoming_string
        data['file_type'] = 'URL'
        dd, num_OK, num_NG , num_warning = process_web(incoming_string)
        
        data['count'] = num_OK + num_warning +num_NG
        data['ng_count'] = num_NG
        data['warning_count'] = num_warning
        data['output'] = dd
    
    # ------------------- LIST OF TEXT ----------------
    # data['test_txt'] = process_text(incoming_string)
    if data_text:
        temp =  []
        data['file_type'] = 'URL'
        for text in data_text:
            dump_data = {
            'data_check': '',
            'reason': '',
            'recommendation':'',
            'result':'',
            'category':'化粧品',
            'marked_x':0,
            'marked_y':0
            }

            text_cls_result,probs = process_text(text['data_check'])
                        
            dump_data['data_check'] = text['data_check']
            dump_data['prob'] = probs
            
            dump_data['result'] = text_cls_result
            dump_data['marked_x'] = text['marked_x']
            dump_data['marked_y'] = text['marked_y']
            if text_cls_result =='NG':
                num_NG += 1
                # num_NG += len(single_line)
            if text_cls_result =='OK':
                # num_OK += 1
                num_OK += len(text['data_check'])
            if text_cls_result =='warning':
                # num_warning += 1
                num_warning += len(text['data_check'])
            temp.append(dump_data)
            
        data['output'] = data['output'] + temp
        data['count'] = data['count'] + num_OK + num_warning +num_NG
        data['ng_count'] += num_NG
        data['warning_count'] += num_warning

    # ------------------- Image Process ----------------
    
    if s3_image is not None:

        print(s3_image,env)
        if env == 'prod':
            bucket_name = 'yakkicheck-prod'
        elif env == 'dev':
            bucket_name = 'yakkicheck-dev'
            
        download_aws(s3_image,bucket_name,s3_image)
        data['url_data']['image_url'] = s3_image
            # print(file_name)
        tt, num_OK, num_NG , num_warning = process_ocr(s3_image)
        data['count'] = data['count'] + num_OK + num_warning +num_NG
        data['ng_count'] += num_NG
        data['warning_count'] += num_warning
        data['output'] = data['output'] + tt

        url_s3 = upload_to_aws(s3_image,'yakkicheck-ai','screenshot/{}'.format(s3_image))
        os.remove(s3_image)
        print(url_s3)
        data['screen_shot'] = url_s3

    print('length of data_output: {}'.format(len(data['output'])))
    if content_type is not None:
        data['file_type'] = content_type
        # fill url
        if content_type == 'IMAGE':
            data['url_data']['image_url'] = file_name
            # print(file_name)
            tt, num_OK, num_NG , num_warning = process_ocr(file_name)
            data['count'] = num_OK + num_warning +num_NG
            data['ng_count'] = num_NG
            data['warning_count'] = num_warning
            data['output'] = tt

            url_s3 = upload_to_aws(file_name,'yakkicheck-ai','screenshot/{}'.format(file_name))
            os.remove(file_name)
            print(url_s3)
            data['screen_shot'] = url_s3
        elif content_type == 'FILE':
            data['url_data']['file_url'] = file_name
            data['screen_shot'] = ''
            data['response'] = ''


        # add responce
        # content of responce    
        data['response'] = 'NONE' 
    # json_dump = json.dumps(data)
    with open('data.json', 'w') as fp:
        json.dump(data, fp)
    url_s3 = upload_to_aws('data.json','yakkicheck-ai','output/{}'.format('data.json'))
    
    return data


def json_generator_admin(input_text):
    # result = BertModel(input_text) #Not Implemented Yet
    result = 'NG'
    # result will be ['OK','gray','N/G'],
    data = {'caterogry':'化粧品',
            'data_check':input_text,
            'result':result
            }
    json_dump = json.dumps(data)
    return data
#     return json_dump
    
def process_text(text):
    try:
        result_text, logit =  text_model(text)
        logit = softmax(torch.squeeze(logit)).cpu().detach().numpy().tolist()
        return result_text , logit
    except:
        return None,None

def process_ocr(image):
    # print(image)
    num_NG = 0
    num_OK = 0
    num_warning = 0 

    out = ocr_reader(image)
    temp =  []
    for data in out:
        # if data[-1] > 0.5:
        dump_data = {
        'data_check': '',
        'reason': '',
        'recommendation':'',
        'result':'',
        'category':'化粧品',
        'marked_x':0,
        'marked_y':0
        }
        text_cls_result,probs= process_text(data[1])
        # text_cls_result= process_text(data[1])
        loc =np.mean(data[0],axis =0)
        # print(temp[0]/w)
        # print(temp[1]/h)
        dump_data['data_check'] = data[1]
        dump_data['prob'] = probs

        dump_data['marked_x'] = loc[1]
        dump_data['marked_y'] = loc[0]
        dump_data['result'] = text_cls_result
        if text_cls_result =='NG':
            num_NG  += 1
            # num_NG  += len(data[1])
        if text_cls_result =='OK':
            # num_OK += 1
            num_OK += len(data[1])
        if text_cls_result =='warning':
            # num_warning += 1
            num_warning += len(single_line)
        temp.append(dump_data)
    return temp  , num_OK, num_NG , num_warning

def process_web(url):
    num_NG = 0
    num_OK = 0
    num_warning = 0 
    try:
        crawl_web(url,'temp_data/imgs')
        # Process text
        texts = []
        jap = re.compile(r'[\u3040-\u309F\u30A0-\u30FF\uAC00-\uD7A3]')
        with open('temp_data/html.txt', "r") as fd:
            for line in fd:
                texts.append(line.strip())
        temp =  []

        print('Done Crawl Text, start Recognize')
        # print(texts)
        for single_line in texts:
            dump_data = {
                'data_check': '',
                'reason': '',
                'recommendation':'',
                'result':'',
                'category':'化粧品',
                'marked_x':'0.5',
                'marked_y':'0.7'
                }
                # print(data[1])
            # if jap.search(single_line) and len(single_line) > 4:
            if len(single_line) > 4:
                text_cls_result,probs = process_text(single_line)
                # text_cls_result = process_text(single_line)
                if text_cls_result is not None:
                    dump_data['result'] = text_cls_result
                    dump_data['prob'] = probs
                    if text_cls_result =='NG':
                        num_NG += 1
                        # num_NG += len(single_line)
                    if text_cls_result =='OK':
                        # num_OK += 1
                        num_OK += len(single_line)
                    if text_cls_result =='warning':
                        # num_warning += 1
                        num_warning += len(single_line)
                    
                    if len(single_line) > 255:
                        
                        dump_data['data_check'] = single_line[:252] + '...'
                    else:
                        dump_data['data_check'] = single_line 
                    
                    temp.append(dump_data)

        # Process image in web
        all_image_files=[]
        for r,d,f in os.walk('temp_data/imgs'):
            if f:
                for file in f:
                    if ('.jpg' in file):
                        exact_path = os.path.join(r,file)
                        all_image_files.append(exact_path)
        
        
        print('Done Crawl Image, start Recognize')
        if all_image_files:
            for single_image in all_image_files:
                
                out = ocr_reader(single_image)
                for data in out:
                    if data[-1] > 0.5:
                        dump_data = {
                        'data_check': '',
                        'reason': '',
                        'recommendation':'',
                        'result':'',
                        'category':'化粧品',
                        'marked_x':'0.5',
                        'marked_y':'0.7'
                        }
                        if len(data[1]) > 2:
                            text_cls_result,probs = process_text(data[1])
                        
                            dump_data['data_check'] = data[1]
                            dump_data['prob'] = probs
                            
                            dump_data['result'] = text_cls_result
                            if text_cls_result =='NG':
                                num_NG += 1
                                # num_NG += len(single_line)
                            if text_cls_result =='OK':
                                # num_OK += 1
                                num_OK += len(single_line)
                            if text_cls_result =='warning':
                                # num_warning += 1
                                num_warning += len(single_line)
                            temp.append(dump_data)
                
        return temp, num_OK, num_NG , num_warning
    except:
        return [],num_OK, num_NG , num_warning

