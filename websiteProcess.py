from urllib.request import urlopen
from bs4 import BeautifulSoup

url = "https://weekly.ascii.jp/"
url = "https://www.yakujihou.com/content/yakkihou.html#:~:text=%E8%96%AC%E6%A9%9F%E6%B3%95%E3%81%A8%E3%81%AF%E3%80%81%E6%AD%A3%E5%BC%8F%E5%90%8D%E7%A7%B0%E3%82%92%E3%80%8C%E5%8C%BB%E8%96%AC%E5%93%81%E3%80%81,%E7%B4%B0%E3%81%8B%E3%81%8F%E5%AE%9A%E3%82%81%E3%81%9F%E6%B3%95%E5%BE%8B%E3%81%A7%E3%81%99%E3%80%82"
html = urlopen(url).read()
soup = BeautifulSoup(html, features="html.parser")

for script in soup(["script", "style"]):
    script.extract()    

text = soup.get_text()
lines = (line.strip() for line in text.splitlines())
chunks = (phrase.strip() for line in lines for phrase in line.split("  "))
text = '\n'.join(chunk for chunk in chunks if chunk)

with open('/output/html.txt', 'w') as f:
    f.write(text)

def crawl(url):
    html = urlopen(url).read()
    soup = BeautifulSoup(html, features="html.parser")

    for script in soup(["script", "style"]):
        script.extract()    

    text = soup.get_text()
    lines = (line.strip() for line in text.splitlines())
    chunks = (phrase.strip() for line in lines for phrase in line.split("  "))
    text = '\n'.join(chunk for chunk in chunks if chunk)
    
